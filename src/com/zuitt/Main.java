package com.zuitt;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scannerName = new Scanner (System.in);
        System.out.println("First Name:");
        String firstName = scannerName.nextLine();
        System.out.println("Last Name:");
        String lastName = scannerName.nextLine();
        System.out.println("1st Grade:");
        double firstGrade = scannerName.nextDouble();
        System.out.println("2nd Grade:");
        double secondGrade = scannerName.nextDouble();
        System.out.println("3rd Grade:");
        double thirdGrade = scannerName.nextDouble();
        int averageGrade = (int)Math.round(firstGrade + secondGrade + thirdGrade)/3;
        System.out.println("Good day, "+ firstName + " " + lastName+ " Your grade average is: "+ averageGrade);
    }
}
